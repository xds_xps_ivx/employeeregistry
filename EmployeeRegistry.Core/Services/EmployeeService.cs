﻿using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Models.Dto;

namespace EmployeeRegistry.Core.Services;

/// <summary>
/// Implements a employee service that reads and writes data in an
/// <see cref="IEmployeeRepository"/> instance.
/// </summary>
public class EmployeeService : IEmployeeService
{
    /// <summary>
    /// Contains a collection of validations to be performed on POST request to
    /// create new employees. Applied against <see cref="EmployeeDto"/> objects.
    /// </summary>
    /// <remarks>
    /// For failing validations, the methods must return
    /// <see langword="true"/>, whereas for passing validations they must
    /// return <see langword="false"/>.
    /// </remarks>
    private static readonly Func<EmployeeDto, bool>[] postContracts =
    {
        p => p is null,
        p => string.IsNullOrWhiteSpace(p.Name),
        p => p.DateOfBirth is null || p.DateOfBirth > DateTime.Today,
    };
    private readonly IEmployeeRepository repository;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeeService"/> class.
    /// </summary>
    /// <param name="repository">
    /// Repository instance to use when retrieving and/or saving data.
    /// </param>
    public EmployeeService(IEmployeeRepository repository)
    {
        this.repository = repository;
    }

    /// <inheritdoc/>
    public ServiceResult<int> Create(EmployeeDto employee)
    {
        if (postContracts.Any(p => p.Invoke(employee)))
        {
            return new ServiceResult<int>(false, "The request to create a new employee is missing some data.");
        }
        Employee newEmployee = new()
        {
            Name = employee.Name!,
            DateOfBirth = employee.DateOfBirth!.Value,
        };
        try
        {
            var result = repository.Create(newEmployee);
            return result > 0
                ? result
                : new ServiceResult<int>(false, "Error while creating the new employee.");
        }
        catch (Exception ex)
        {
            return new ServiceResult<int>(false, ex.Message);
        }
    }

    /// <inheritdoc/>
    public ServiceResult Delete(int employeeId)
    {
        if (Read(employeeId) is { Success: true })
        {
            repository.Delete(employeeId);
            return true;
        }
        else
        {
            return new ServiceResult(false, "Employee not found");
        };
    }

    /// <inheritdoc/>
    public ServiceResult<Employee> Read(int employeeId)
    {
        return repository.Read(employeeId) is { } employee
            ? employee
            : new ServiceResult<Employee>(false, "Employee not found");
    }

    /// <inheritdoc/>
    public ServiceResult Update(int employeeId, EmployeeDto data)
    {
        if (Read(employeeId) is { Success: true, Result: { } updatedEmployee })
        {
            bool changesMade = false;
            void SetIfNotNull<T>(T? value, Func<Employee, T> currentValue, Action<Employee, T> setter) where T : notnull
            {
                if (value is not null && !value.Equals(currentValue.Invoke(updatedEmployee)))
                { 
                    changesMade = true;
                    setter.Invoke(updatedEmployee, value);
                }
            }

#pragma warning disable CS8714
            SetIfNotNull(data.Name, p => p.Name, (p, v) => p.Name = v);
            SetIfNotNull(data.DateOfBirth, p => p.DateOfBirth, (p, v) => p.DateOfBirth = v!.Value);
#pragma warning restore CS8714

            if (changesMade)
            {
                repository.Update(updatedEmployee);
            }
            return new ServiceResult(true, changesMade ? null : "No changes were made");
        }
        else
        { 
            return new ServiceResult(false, "Employee not found");
        };
    }

    /// <inheritdoc/>
    public IEnumerable<Employee> GetAll()
    {
        return repository.GetAll();
    }
}
