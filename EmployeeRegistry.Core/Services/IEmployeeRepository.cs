﻿using EmployeeRegistry.Core.Models;

namespace EmployeeRegistry.Core.Services;

/// <summary>
/// Defines a set of members to be implemented by a type that exposes methods
/// to read and write employees in a data repository.
/// </summary>
public interface IEmployeeRepository : IDisposable
{
    /// <summary>
    /// Adds the employee into the repository data store.
    /// </summary>
    /// <param name="employee">Movie to add.</param>
    /// <returns>The ID of the newly created employee.</returns>
    public int Create(Employee employee);

    /// <summary>
    /// Reads a employee with the specified Id from the data store.
    /// </summary>
    /// <param name="employeeId">employee Id to read.</param>
    /// <returns>
    /// A employee with the specified Id, or <see langword="null"/> if no such
    /// employee exists.
    /// </returns>
    public Employee? Read(int employeeId) => GetAll().SingleOrDefault(p => p.EmployeeId == employeeId);

    /// <summary>
    /// Updates a employee already present in the data store.
    /// </summary>
    /// <param name="data">
    /// Data to be set onto an existing entity in the data store.
    /// </param>
    public void Update(Employee data);

    /// <summary>
    /// Deletes a employee with the specified Id from the data store.
    /// </summary>
    /// <param name="employeeId">Id of the employee to be deleted.</param>
    public void Delete(int employeeId);

    /// <summary>
    /// Gets a queryable object that may be used to construct queries into the
    /// data store.
    /// </summary>
    /// <returns>
    /// A queryable object that may be used to construct queries into the data
    /// store.
    /// </returns>
    public IQueryable<Employee> GetAll();
}