﻿using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Models.Dto;

namespace EmployeeRegistry.Core.Services;

/// <summary>
/// Defines a set of members to be implemented by a type that can store and
/// retrieve employees.
/// </summary>
public interface IEmployeeService
{
    /// <summary>
    /// Creates a new employee.
    /// </summary>
    /// <param name="employee">Employee DTO with the data to insert.</param>
    /// <returns>
    /// A <see cref="ServiceResult{T}"/> with the employee id of the newly
    /// created employee.
    /// </returns>
    public ServiceResult<int> Create(EmployeeDto employee);

    /// <summary>
    /// Gets an existing employee with the specified id.
    /// </summary>
    /// <param name="employeeId">Employee Id to get.</param>
    /// <returns>
    /// A <see cref="ServiceResult{T}"/> with either the employee that has the
    /// specified employee id or <see langword="null"/> if no such employee has
    /// been found.
    /// </returns>
    public ServiceResult<Employee> Read(int employeeId);

    /// <summary>
    /// Updates an existing employee with the specified data.
    /// </summary>
    /// <param name="employeeId">Employee Id to update.</param>
    /// <param name="data">DTO with the new data to be set.</param>
    /// <returns>
    /// A <see cref="ServiceResult"/> that indicates either success or failure
    /// to perform the operation.
    /// </returns>
    public ServiceResult Update(int employeeId, EmployeeDto data);

    /// <summary>
    /// Deletes the employee with the specified employee id.
    /// </summary>
    /// <param name="employeeId">Employee Id to delete.</param>
    /// <returns>
    /// A <see cref="ServiceResult"/> that indicates either success or failure
    /// to perform the operation.
    /// </returns>
    public ServiceResult Delete(int employeeId);


    /// <summary>
    /// Enumerates all employees.
    /// </summary>
    /// <returns>A collection of all employees</returns>
    IEnumerable<Employee> GetAll();

}
