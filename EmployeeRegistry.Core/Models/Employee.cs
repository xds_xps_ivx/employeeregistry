﻿namespace EmployeeRegistry.Core.Models;

/// <summary>
/// Defines a employee.
/// </summary>
public class Employee
{
    /// <summary>
    /// Gets or sets the employee id.
    /// </summary>
    public int EmployeeId { get; set; }

    /// <summary>
    /// Gets or sets the employee name.
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    /// Gets or sets the employee's date of birth.
    /// </summary>
    public DateTime DateOfBirth { get; set; }
}

