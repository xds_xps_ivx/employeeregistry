﻿namespace EmployeeRegistry.Core.Models.Dto;

/// <summary>
/// Defines a employee DTO that may be used to perform Create/Update operations.
/// </summary>
public class EmployeeDto
{
    /// <summary>
    /// Gets or sets the employee name.
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// Gets or sets the employee's date of birth.
    /// </summary>
    public DateTime? DateOfBirth { get; set; }
}
