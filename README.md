# Employee Registry
This is a test project that includes very basic functionality to list and add employee entities to a database, either using Entity Framework or Dapper.

## Building `EmployeeRegistry`
`EmployeeRegistry` can be built on any platform or CI environment supported by dotnet.

The WebAPI component uses dependency injection to get the required services, and it currently includes support for `EmployeeRegistry.EfCore` and `EmployeeRegistry.Dapper`, based on `EntityFrameworkCore` and `Dapper` respectively.

### Prerequisites
- [.Net SDK 6.0](https://dotnet.microsoft.com/).
- [.Net SDK 7.0 or later](https://dotnet.microsoft.com/) (needed for C#11 support).
- [Node.js v18.19.0 or later](https://nodejs.org/en/download/) (the latest LTS should be fine)

If trying out the Dapper provider:
- [dotnet-sqlpackage](https://learn.microsoft.com/sql/tools/sqlpackage/sqlpackage-download) (to deploy the database)
- SQL Server 2016 or later (Express or LocalDB will work)

### Building the solution
By default, the solution will build using `EmployeeRegistry.EfCore`, running with an in-memory database, which might not be ideal for some testing scenarios.

To build using the default `EfCore` provider, run:
```sh
dotnet build ./EmployeeRegistry.sln
```
To switch defaults, edit the `EmployeeRegistry.Server.csproj` file, and change the line 14 to:
```xml
	<UseDapper>true</UseDapper>
```
> NOTE: If no default is set and no provider is specified, the solution will fail to build.

#### Using `EmployeeRegistry.EfCore`
To explicitly use the `EfCore` provider, run:
```sh
dotnet build ./EmployeeRegistry.sln -p:UseEfCore=true
```
#### Using `EmployeeRegistry.Dapper`
To explicitly use the `Dapper` provider, run:
```sh
dotnet build ./EmployeeRegistry.sln -p:UseDapper=true
```
> NOTE: Make sure to have the database defined in the `EmployeeRegistry.Sql` project deployed, and adjust the connection string accordingly in `appsettings.Development.json`

### Deploying the databse (If using Dapper)
In case you're using Dapper as the repository provider, you'll need to deploy the database at least once. When using `EfCore` this is not required due to it using a database in memory. Moreover, Entity Framework Core can create the DB schema upon service execution by itself without requiring manual database deployments.

Make sure you have the `dotnet-sqlpackage` tool:
```sh
dotnet tool install -g microsoft.sqlpackage
```
Compile the solution with `Dapper`, and use the `dotnet-sqlpackage` tool to deploy the database.
> In this example, we're publishing to a LocalDb instance on the local computer. Adjust the specified connection string as required, and edit the `appsettings.Development.json` file in the `EmployeeRegistry.Server` project.
```sh
dotnet build ./EmployeeRegistry.sln -p:UseDapper=true
SqlPackage -Action:Publish -SourceFile:"./EmployeeRegistry.Sql/bin/Debug/EmployeeRegistry.Sql.dacpac" -TargetConnectionString:"Server=(localdb)\mssqllocaldb;Database=EmployeeRegistry;Trusted_Connection=True;"
```
> Note: this is a `PowerShell` command, adjust the connection string characters if you're using `bash`.

Alternatively, you may deploy the database project using Visual Studio.

### Running unit tests
```sh
dotnet test ./EmployeeRegistry.sln
```

## Running `EmployeeRegistry`
`EmployeeRegistry` is split into two parts. The client app is a React app running on Node.js, while the back-end is an ASP.NET WebAPI service.

### Running from Visual Studio
> This option only applies when running from a Windows environment
You can run the solution from Visual Studio. To do so, you need to set up multiple startup projects:
- `EmployeeRegistry.Server` should be set as the first project to start.
- `employeeregistry.client` should be set as the second project to start.
Once the startup projects have been set up, you'll be able to run the solution by clicking `Start` or by pressing `F5`

### Running from the terminal
You'll need to execute two terminal instances.

The first server you need to start is `EmployeeRegistry.Server`. 

To explicitly use the `EfCore` provider, run:
```sh
dotnet run --project ./EmployeeRegistry.Server/EmployeeRegistry.Server.csproj -p:UseEfCore=true
```

To explicitly use the `Dapper` provider, run:
```sh
dotnet run --project ./EmployeeRegistry.Server/EmployeeRegistry.Server.csproj  -p:UseDapper=true
```
> NOTE: Make sure to have the database defined in the `EmployeeRegistry.Sql` project deployed, and adjust the connection string accordingly in `appsettings.Development.json`. See [above](#deploying-the-databse-if-using-dapper))


After executing the server, you must start the client service on another terminal instance:
```sh
npm run --prefix ./employeeregistry.client/ dev
```
The webapp client service will indicate the URL you can navigate to in order to see the running application (by default, set to [https://localhost:5173/](https://localhost:5173/)).