export function getDefaultEmployeeFormData() {
    return { name: '', dateOfBirth: new Date() }
}
export async function createEmployee(newEmployee) {
    try {
        const r = await fetch('http://localhost:5265/employees', {
            method: 'POST',
            headers: {
                "Content-Type": 'application/json',
            },
            body: JSON.stringify(newEmployee),
        })
        return r
    } catch (e) {
        console.log(e)
    }
}

export async function getEmployee(employeeId) {
    try {
        const r = await fetch(`http://localhost:5265/employees/${employeeId}`)
        console.log(r)
        return await r.json()
    }
    catch (e) {
        console.log(e)
    }
}