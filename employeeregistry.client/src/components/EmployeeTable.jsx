import '../App.css';

export default function EmployeeTable({ employees }) {
    return (<table className="table table-striped" aria-labelledby="tabelLabel">
        <thead>
            <tr>
                <th>Employee ID</th>
                <th>Name</th>
                <th>Date of birth</th>
            </tr>
        </thead>
        <tbody>
            {employees.map((employee) =>
                <tr key={employee.name}>
                    <td>{employee.employeeId}</td>
                    <td>{employee.name}</td>
                    <td>{employee.dateOfBirth}</td>
                </tr>
            )}
        </tbody>
    </table>)
}
