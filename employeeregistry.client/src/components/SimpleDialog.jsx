import '../App.css';
export default function SimpleDialog({ onClose, show, children }) {
    const showHide = show ? "modal display-block" : "modal display-none";
    return (
        <div className={showHide}>
            <section className="modal-main">
                {children}
                <button type="button" onClick={onClose}>
                    Close
                </button>
            </section>
        </div>
    );
}