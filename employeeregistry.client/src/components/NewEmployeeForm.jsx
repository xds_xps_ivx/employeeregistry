import { useState } from 'react';
import '../App.css';
import { getDefaultEmployeeFormData, createEmployee } from '../services/employeeService.js'
import SimpleDialog from './SimpleDialog'
export default function NewEmployeeForm({ employees, setEmployees }) {
    const [newItem, setNewItem] = useState(getDefaultEmployeeFormData());
    const [dialogVisible, setDialogVisible] = useState(false);
    async function createNewEmployee() {
        const r = await createEmployee(newItem)
        if (r.ok) {
            const newId = await r.text()
            setEmployees([...employees, { ...newItem, employeeId: newId }]);
            setNewItem(getDefaultEmployeeFormData());
            setDialogVisible(true)
        }
    }

    return (<div className="form">
        <SimpleDialog show={dialogVisible} onClose={() => setDialogVisible(false)}>
            <p>Employee created successfully.</p>
        </SimpleDialog>
        <h2>Add new employee</h2>
        <div>
            <div className="form-grid">
                <label htmlFor="name">Name:</label>
                <input
                    type="text"
                    id="name"
                    onChange={(e) => { setNewItem({ ...newItem, name: e.target.value }) }}
                    value={newItem.name} />
            </div>
            <div className="form-grid">
                <label htmlFor="description">Date of birth:</label>
                <input
                    type="date"
                    id="dob"
                    onChange={(e) => { setNewItem({ ...newItem, dateOfBirth: e.target.value }) }}
                    value={newItem.description} />
            </div>
        </div>
        <button onClick={createNewEmployee}>Create</button>
    </div>)
}
