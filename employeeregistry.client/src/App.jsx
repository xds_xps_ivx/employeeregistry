import { useEffect, useState } from 'react';
import './App.css';
import NewEmployeeForm from './components/NewEmployeeForm';
import EmployeeTable from './components/EmployeeTable';
import { getEmployee } from './services/employeeService';

export default function App() {
    const [errorMessage, setErrorMessage] = useState('');
    const [employees, setEmployees] = useState([]);
    const [filter, setFilter] = useState('');

    const handleSearch = (event) => {
        const filter = event.target.value;
        setFilter(filter);
    };

    const filterItems = () => {
        if (filter === '') {
            return employees;
        }
        else {
            return employees.filter(
                (item) => item.name.toLowerCase().includes(filter.toLowerCase())
            );
        }
    };

    const fetchEmployees = () => {
        setErrorMessage('Loading...')
        getEmployee('all').then(c => {
            setErrorMessage('')
            setEmployees(c)
        }).catch(r => setErrorMessage(`Error fetching employee list. (Bad CORS policy, or server not running?)\n${r}`))
    }

    const updateEmployees = (employees) => {
        setEmployees(employees)
    }

    const contents = errorMessage != ''
        ? <p><em>{errorMessage}</em></p>
        : <div>
            <h2>Search</h2>
            <div className="search form-grid">
                <label htmlFor="filter">Filter:</label>
                <input type="text" id="filter" value={filter} onChange={handleSearch} />
            </div>
            <EmployeeTable employees={filterItems()} />
        </div>


    useEffect(() => { fetchEmployees() }, [])

    return (
        <div className="App">
            <h1 id="tabelLabel">Employee registry</h1>
            <p>This is an employee registry.</p>
            <NewEmployeeForm employees={employees} setEmployees={updateEmployees} />
            {contents}
        </div>
    );
}