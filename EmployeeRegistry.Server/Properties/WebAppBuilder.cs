﻿using EmployeeRegistry.Core.Services;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Reflection;
using Microsoft.OpenApi.Models;

#if UseEfCore
using Microsoft.EntityFrameworkCore;
using EmployeeRegistry.EfCore.Data;
using EmployeeRegistry.EfCore.Models;
#elif UseDapper
using EmployeeRegistry.Dapper;
#endif

namespace EmployeeRegistry.Server.Properties;

internal static class WebAppBuilder
{
    public static WebApplication BuildWebApp()
    {
        var app = ConfigureServices().Build();

        app.UseDefaultFiles();
        app.UseStaticFiles();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseAuthentication();
        app.UseRouting();
        app.UseAuthorization();
        app.MapControllers();
        app.MapFallbackToFile("/index.html");
        app.UseCors(x => x
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .SetIsOriginAllowed(origin => true)
                    .AllowCredentials());
        return app;
    }

    private static WebApplicationBuilder ConfigureServices()
    {
        var builder = WebApplication.CreateBuilder(Environment.GetCommandLineArgs());
        var connectionString = builder.Configuration.GetConnectionString("EmployeeRegistry");
#if UseEfCore
        builder.Services.AddDbContext<EmployeeRegistryContext>(options => options.UseInMemoryDatabase(nameof(EmployeeRegistryContext)));
#elif UseDapper
        builder.Services.AddSingleton<IConnectionFactory>(new ConnectionFactory(connectionString));        
#endif
        builder.Services.AddTransient<IEmployeeRepository, EmployeeRepository>();
        builder.Services.AddTransient<IEmployeeService, EmployeeService>();
        builder.Services.AddControllers();
        builder.Services.AddEndpointsApiExplorer();
#if IncludeAuthorization
        builder.Services.AddAuthentication("Bearer").AddJwtBearer().AddJwtBearer("LocalAuthIssuer");
#endif
        builder.Services.AddSwaggerGen(SetupSwagger);

        return builder;
    }

    private static void SetupSwagger(SwaggerGenOptions options)
    {
        options.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Employee Registry API",
            Description = "An ASP.NET Core Web API for managing employees",
        });
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
    }
}
