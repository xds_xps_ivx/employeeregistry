﻿using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Services;

#if UseEfCore
using Microsoft.EntityFrameworkCore;
using EmployeeRegistry.EfCore.Data;
using EmployeeRegistry.EfCore.Models;
#elif UseDapper
using EmployeeRegistry.Dapper;
#endif

namespace EmployeeRegistry.Server;

/// <summary>
/// Class that allows for data seeding on a test or empty database.
/// </summary>
/// <remarks>For testing purposes only.</remarks>
public static class TestFill
{
    /// <summary>
    /// Populates the database with random test data.
    /// </summary>
    /// <param name="qty">Amount of entries to generate.</param>
    /// <param name="configuration">
    /// Configuration to use when creating the required repository connection.
    /// </param>
    public static void PopulateTestDb(int qty, IConfiguration configuration)
    {
        using var svc = GetRepository(configuration);
        if (!svc.GetAll().Any())
        {
            foreach (var movie in Enumerable.Range(1, qty).Select(GetFullEmployee))
            {
                svc.Create(movie);
            }
        }
    }

    private static IEmployeeRepository GetRepository(IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("EmployeeRegistry");
#if UseEfCore
        var options = new DbContextOptionsBuilder();
        options.UseInMemoryDatabase(nameof(EmployeeRegistryContext));
        var context = new EmployeeRegistryContext(options.Options);
        return new EmployeeRepository(context);
#elif UseDapper
        var f = new ConnectionFactory(connectionString);
        return new EmployeeRepository(f);
#endif
    }

    private static readonly Random random = new();

    private static Employee GetFullEmployee(int id)
    {
        return new Employee()
        {
            EmployeeId = id,
            DateOfBirth = DateTime.UtcNow.Date - TimeSpan.FromDays(random.Next((int)(21 * 365.25), (int)(60 * 365.25))),
            Name = $"Employee {id}",
        };
    }
}