using Microsoft.AspNetCore.Mvc;
using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Services;
using EmployeeDto = EmployeeRegistry.Core.Models.Dto.EmployeeDto;

#if IncludeAuthorization
using Microsoft.AspNetCore.Authorization;
#endif

namespace EmployeeRegistry.Api.Controllers;

/// <summary>
/// Defines a controller to manage a employees repository.
/// </summary>
[ApiController]
[Route("[controller]")]
#if IncludeAuthorization
[Authorize]
#endif
public class EmployeesController : ControllerBase
{
    private readonly IEmployeeService service;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeesController"/> class.
    /// </summary>
    /// <param name="service">
    /// <see cref="IEmployeeService"/> dependency to be injected into this
    /// instance.
    /// </param>
    public EmployeesController(IEmployeeService service)
    {
        this.service = service;
    }

    /// <summary>
    /// Creates a new employee entry.
    /// </summary>
    /// <param name="employee">
    /// Data of the new employee to be created.
    /// </param>
    /// <returns>The Id of the newly created employee.</returns>
    /// <response code="201">
    /// Returns the id of th newly created employee.
    /// </response>
    /// <response code="400">
    /// Returned if the employee could not be created due to incomplete data.
    /// </response>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult Post([FromBody] EmployeeDto employee)
    {
        return service.Create(employee) is { Success: true } result
            ? Created($"1", result.Result)
            : BadRequest();
    }

    /// <summary>
    /// Updates an existing employee.
    /// </summary>
    /// <param name="employeeId">Id of the employee to be updated.</param>
    /// <param name="employee">Data to update onto the employee.</param>
    /// <returns>
    /// A response that rescribes either success or failure to perform the
    /// operation.
    /// </returns>
    /// <response code="204">
    /// No data is returned.
    /// </response>
    /// <response code="400">
    /// Returned if the employee could not be updated due to invalid data.
    /// </response>
    [HttpPatch("{employeeId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult Patch(int employeeId, [FromBody] EmployeeDto employee)
    {
        var readResult = service.Read(employeeId);
        if (!readResult.Success) return NotFound();

        return service.Update(employeeId, employee) is { Success: true }
            ? Ok()
            : BadRequest();
    }

    /// <summary>
    /// Gets a paged array of all employees.
    /// </summary>
    /// <param name="page">Page number.</param>
    /// <param name="itemsPerPage">Items per page to get.</param>
    /// <returns>
    /// An array of all employees in the specified page number.
    /// </returns>
    /// <response code="200">
    /// Returns an array of employees is returned.
    /// </response>
    [HttpGet("All")]
    [Produces("application/json")]
    public ActionResult<Employee[]> All([FromQuery] int? page, [FromQuery] int? itemsPerPage)
    {
        if (page.HasValue || itemsPerPage.HasValue)
        {
            return service.GetAll().Skip(((page ?? 1) - 1) * (itemsPerPage ?? 10)).Take(itemsPerPage ?? 10).ToArray();
        }
        else
        {
            return service.GetAll().ToArray();
        }
    }

    /// <summary>
    /// Gets a count of pages based on the <paramref name="itemsPerPage"/>
    /// query.
    /// </summary>
    /// <param name="itemsPerPage">Number of items per page.</param>
    /// <returns>
    /// The number of pages based on how many items per page should be
    /// displayed.
    /// </returns>
    /// <response code="200">
    /// Returns the number of pages that would exist if displaying the
    /// specified number of items per page.
    /// </response>
    /// <response code="400">
    /// Returned if itemsPerPage is either not specified or less than 1.
    /// </response>
    [HttpGet("NumberOfPages")]
    [Produces("text/plain", Type = typeof(int))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<int> NumberOfPages([FromQuery] int itemsPerPage)
    {
        if (itemsPerPage <= 0) return BadRequest();
        var c = service.GetAll().Count();
        return (c / itemsPerPage) + (c % itemsPerPage != 0 ? 1 : 0);
    }

    /// <summary>
    /// Reads a single employee item.
    /// </summary>
    /// <param name="employeeId">Id of the employee to read.</param>
    /// <returns>A employee from the repository.</returns>
    /// <response code="200">
    /// Returns a employee.
    /// </response>
    /// <response code="404">
    /// Returned if a employee with the specified id does not exist.
    /// </response>
    [HttpGet("{employeeId}")]
    [Produces("application/json")]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<Employee> Get(int employeeId)
    {
        return service.Read(employeeId) is { Success: true, Result: { } employee } ? new ActionResult<Employee>(employee) : NotFound();
    }

    /// <summary>
    /// Deletes a employee from the repository.
    /// </summary>
    /// <param name="employeeId">Id of the employee to be deleted.</param>
    /// <returns>
    /// A result that indicates either success or failure to perform the
    /// operation.
    /// </returns>
    /// <response code="204">
    /// No data is returned.
    /// </response>
    /// <response code="404">
    /// Returned if a employee with the specified id does not exist.
    /// </response>
    [HttpDelete("{employeeId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult Delete(int employeeId)
    {
        return service.Delete(employeeId) is { Success: true } ? Ok() : NotFound();
    }
}