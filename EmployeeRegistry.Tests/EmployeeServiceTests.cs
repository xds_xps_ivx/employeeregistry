using Moq;
using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Services;
using EmployeeRegistry.Core.Models.Dto;

namespace EmployeeRegistry.Tests;

public class EmployeeServiceTests
{
    private static Employee GetFullEmployee(int id)
    {
        return new Employee()
        {
            EmployeeId = id,
            Name = $"Employee {id}",
            DateOfBirth = new DateTime(1980 + id, (id % 12) + 1, (id % 28) + 1)
        };
    }

    private static IEnumerable<EmployeeDto> PartialEmployeeDtos()
    {
        yield return new EmployeeDto()
        {
            Name = "Test",
        };
        yield return new EmployeeDto()
        {
            DateOfBirth = new DateTime(1980, 1, 1),
        };
    }

    [Test]
    public void Service_can_create_employees()
    {
        var newEmployee = new EmployeeDto()
        {
            Name = "Test",
            DateOfBirth = new DateTime(1980, 1, 1)
        };        
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Create(It.IsAny<Employee>())).Returns(1).Verifiable(Times.Once());

        var service = new EmployeeService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(newEmployee).Success, Is.True);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [TestCaseSource(nameof(PartialEmployeeDtos))]
    public void Create_fails_if_data_is_missing(EmployeeDto failingDto)
    {
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Create(It.IsAny<Employee>())).Verifiable(Times.Never());

        var service = new EmployeeService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(failingDto).Success, Is.False);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Create_fails_on_repo_error()
    {
        var newEmployee = new EmployeeDto()
        {
            Name = "Test",
            DateOfBirth = new DateTime(1980, 1, 1)
        };
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Create(It.IsAny<Employee>())).Throws<IOException>().Verifiable(Times.Once());

        var service = new EmployeeService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Create(newEmployee).Success, Is.False);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_update_employees()
    {
        var newData = new EmployeeDto() { Name = "Test2" };
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullEmployee(1)).Verifiable(Times.Once());
        repo.Setup(r => r.Update(It.IsAny<Employee>())).Verifiable(Times.Once());

        var service = new EmployeeService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_skips_update_if_no_changes_declared()
    {
        var newData = new EmployeeDto();
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullEmployee(1));
        repo.Setup(r => r.Update(It.IsAny<Employee>())).Verifiable(Times.Never());
        var service = new EmployeeService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
        repo.Verify();
    }

    [Test]
    public void Service_skips_update_if_no_actual_changes()
    {
        var newData = new EmployeeDto() { Name = GetFullEmployee(1).Name };
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullEmployee(1));
        repo.Setup(r => r.Update(It.IsAny<Employee>())).Verifiable(Times.Never());
        var service = new EmployeeService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
        repo.Verify();
    }

    [Test]
    public void Update_fails_if_employee_does_not_exists()
    {
        var newData = new EmployeeDto() { Name = "Test2" };
        var repo = new Mock<IEmployeeRepository>();
        var employee = GetFullEmployee(1);
        repo.Setup(r => r.Read(1)).Returns((Employee?)null).Verifiable(Times.Once());
        repo.Setup(r => r.Update(employee)).Verifiable(Times.Never());

        var service = new EmployeeService(repo.Object);
        var result = service.Update(1, newData);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.False);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_read_employees()
    {
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1))
            .Returns(GetFullEmployee(1))
            .Verifiable(Times.Once());
        var service = new EmployeeService(repo.Object);
        var result = service.Read(1);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.True);
            Assert.That(result.Result, Is.InstanceOf<Employee>());
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Read_fails_if_employee_does_not_exist()
    {
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1))
            .Returns((Employee?)null)
            .Verifiable(Times.Once());
        var service = new EmployeeService(repo.Object);
        var result = service.Read(1);
        Assert.Multiple(() =>
        {
            Assert.That(result.Success, Is.False);
            Assert.That(result.Message, Is.Not.Null);
            Assert.That(result.Result, Is.Null);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }

    [Test]
    public void Service_can_delete_employees()
    {
        var repo = new Mock<IEmployeeRepository>();
        repo.Setup(r => r.Read(1)).Returns(GetFullEmployee(1)).Verifiable(Times.Once());
        repo.Setup(r => r.Delete(1)).Verifiable(Times.Once());

        var service = new EmployeeService(repo.Object);
        Assert.Multiple(() =>
        {
            Assert.That(service.Delete(1).Success, Is.True);
            Assert.That(() => repo.Verify(), Throws.Nothing);
        });
    }
}