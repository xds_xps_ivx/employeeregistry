﻿using Dapper;
using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Services;
using System.Data.SqlClient;

namespace EmployeeRegistry.Dapper;

/// <summary>
/// Implements the <see cref="IEmployeeRepository"/> interface on a type that
/// uses Dapper for SQL operations.
/// </summary>
public class EmployeeRepository : IEmployeeRepository
{
    private readonly SqlConnection connection;
    private bool disposedValue;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeeRepository"/>
    /// class.
    /// </summary>
    /// <param name="connectionFactory">
    /// Connection factory instance to use when creating the required SQL
    /// connection.
    /// </param>
    public EmployeeRepository(IConnectionFactory connectionFactory)
    {
        connection = connectionFactory.CreateConnection();
    }

    /// <inheritdoc/>
    public int Create(Employee employee)
    {
        return Convert.ToInt32(connection.ExecuteScalar(
            """
            INSERT INTO Employees (Name, DateOfBirth)
            OUTPUT INSERTED.EmployeeId
            VALUES (@Name, @DateOfBirth);
            """, employee)!);
    }

    /// <inheritdoc/>
    public Employee Read(int employeeId)
    {
        return connection.QueryFirstOrDefault<Employee>("SELECT * FROM Employees WHERE EmployeeId = @id", new { id = employeeId })!;
    }

    /// <inheritdoc/>
    public void Update(Employee data)
    {
        connection.Execute("UPDATE Employees SET Name = @Name, DateOfBirth = @DateOfBirth WHERE EmployeeId = @EmployeeId", data);
    }

    /// <inheritdoc/>
    public void Delete(int employeeId)
    {
        connection.Execute("DELETE FROM Employees WHERE EmployeeId = @id", new { id = employeeId });
    }

    /// <inheritdoc/>
    public IQueryable<Employee> GetAll()
    {
        return connection.Query<Employee>("SELECT * FROM Employees").AsQueryable();
    }

    /// <summary>
    /// Performs the required operations to dispose this instance.
    /// </summary>
    /// <param name="disposing">
    /// Indicates whether or not to dispose internal objects that implement
    /// <see cref="IDisposable"/>.
    /// </param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                connection.Dispose();
            }
            disposedValue = true;
        }
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
