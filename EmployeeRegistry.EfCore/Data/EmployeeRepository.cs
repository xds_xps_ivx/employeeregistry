﻿using EmployeeRegistry.Core.Models;
using EmployeeRegistry.Core.Services;
using EmployeeRegistry.EfCore.Models;

namespace EmployeeRegistry.EfCore.Data;

/// <summary>
/// Implements a <see cref="IEmployeeRepository"/> that uses Entity Framework to
/// perform operations against a database.
/// </summary>
public class EmployeeRepository : IEmployeeRepository
{
    private readonly EmployeeRegistryContext context;
    private bool disposedValue;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeeRepository"/> class.
    /// </summary>
    /// <param name="context">Context to perform data operations onto.</param>
    public EmployeeRepository(EmployeeRegistryContext context)
    {
        context.Database.EnsureCreated();
        this.context = context;
    }

    /// <inheritdoc/>
    public int Create(Employee employee)
    {
        context.Employees.Add(employee);
        context.SaveChanges();
        return employee.EmployeeId;
    }

    /// <inheritdoc/>
    public void Delete(int employeeId)
    {
        if (context.Employees.Find(employeeId) is { } employee)
        {
            context.Employees.Remove(employee);
            context.SaveChanges();
        }
    }

    /// <inheritdoc/>
    public IQueryable<Employee> GetAll()
    {
        return context.Employees;
    }

    /// <inheritdoc/>
    public void Update(Employee data)
    {
        context.Update(data);
        context.SaveChanges();
    }

    /// <summary>
    /// Performs the required operations to dispose this instance.
    /// </summary>
    /// <param name="disposing">
    /// Indicates whether or not to dispose internal objects that implement
    /// <see cref="IDisposable"/>.
    /// </param>
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                context.Dispose();
            }
            disposedValue = true;
        }
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
