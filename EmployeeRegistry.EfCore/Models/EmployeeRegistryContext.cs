﻿using Microsoft.EntityFrameworkCore;
using EmployeeRegistry.Core.Models;

namespace EmployeeRegistry.EfCore.Models;

/// <summary>
/// Defines the data context to be used when interacting with a database.
/// </summary>
public class EmployeeRegistryContext : DbContext
{
    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeeRegistryContext"/>
    /// class.
    /// </summary>
    public EmployeeRegistryContext()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="EmployeeRegistryContext"/>
    /// class.
    /// </summary>
    /// <param name="options">Options to use when creating the model.</param>
    public EmployeeRegistryContext(DbContextOptions options) : base(options)
    {
    }

    /// <summary>
    /// Defines a dataset for <see cref="Employee"/> entities.
    /// </summary>
    public DbSet<Employee> Employees { get; set; }
}